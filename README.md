Builds an API with Node/Express/MikroORM. Tested with Postman. DB on ElephantSQL.

Needs `.env` with `ELEPHANTSQL_URL`.