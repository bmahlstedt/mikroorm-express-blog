const allEntities = require('./server/entities');
require('dotenv').config();

module.exports = {
  entities: allEntities,
  type: 'postgresql',
  clientUrl: process.env.ELEPHANTSQL_URL,
}